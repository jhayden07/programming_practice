#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

void PrintList(std::vector<int> SourceList);
std::vector<int> MergeSort(std::vector<int> & List);
std::vector<int> Merge(std::vector<int> & List1, std::vector<int> & List2);

void main(void)
{
	std::vector<int>   List1 = { 4, 15, 16, 50, 8, 23, 42, 108, 4 };
	std::vector<int> MergeSortedList = MergeSort(List1);

	PrintList(List1);
	PrintList(MergeSortedList);

	std::cin.get();
}

void PrintList(std::vector<int> SourceList)
{
	for (int Item : SourceList)
	{
		std::cout << Item << " ";
	}

	std::cout << ::endl;
}

std::vector<int> MergeSort(std::vector<int> & List)
{
	if (List.size() != 1)
	{
		int two = 2;
		// Calculate middle element and round down        
		std::vector<int>::iterator  ListMiddle = List.begin() + (List.size() / 2);
		std::vector<int> LeftSide(List.begin(), ListMiddle);
		std::vector<int> RightSide(ListMiddle, List.end());

		LeftSide = MergeSort(LeftSide);
		RightSide = MergeSort(RightSide);
		return Merge(LeftSide, RightSide);

	}
	else {
		return List;
	}
}


std::vector<int> Merge(std::vector<int> & List1, std::vector<int> & List2)
{
	std::vector<int> SortedList;
	//merge list
	while (List1.empty() != true && List2.empty() != true)
	{
		if (List1[0] < List2[0])
		{
			SortedList.push_back(List1[0]);
			List1.erase(List1.begin());
		}
		else {
			SortedList.push_back(List2[0]);
			List2.erase(List2.begin());
		}
	}

	//check if l1 is still not empty
	while (!List1.empty())
	{
		// add current 
		SortedList.push_back(List1[0]);
		//delete the insert from original list
		List1.erase(List1.begin());
	}

	//check if l1 is still not empty
	while (!List2.empty())
	{
		// add current 
		SortedList.push_back(List2[0]);
		//delete the insert from original list
		List2.erase(List2.begin());
	}

	return SortedList;
}
